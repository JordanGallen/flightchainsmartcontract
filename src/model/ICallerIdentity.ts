
export interface ICallerIdentity {
    // Which entity is attempting to updates the data.
    owner: string;
    // single entity may have several subsidiaries that it manages data for
    iataCodes: string[];
}
