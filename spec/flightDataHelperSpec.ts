import {ACRISFlight, PaxCount} from '../src/model/ACRISFlight';

const flightDataHelper = require('../src/flightDataHelper');

describe('flightDataHelper', () => {

    describe('verifyIdentity', () => {
        it('should return concatenation of date, dep  airport, carrier and flight number', async () => {
            const flight = <ACRISFlight> {
                operatingAirline: {iataCode: 'EI'},
                departureAirport: 'JFK',
                arrivalAirport: "DUB",
                originDate: '2019-03-17',
                flightNumber: {trackNumber: '1234'}
            };

            const result = flightDataHelper.FlightDataHelper.getUniqueFlightKey(flight);

            expect(result).toEqual('2019-03-17JFKEI1234');
        });

        it('should prepend zeros to flight number when less than 4 chars', async () => {
            const flight = <ACRISFlight> {
                operatingAirline: {iataCode: 'EI'},
                departureAirport: 'JFK',
                arrivalAirport: "DUB",
                originDate: '2019-03-17',
                flightNumber: {trackNumber: '34'}
            };

            const result = flightDataHelper.FlightDataHelper.getUniqueFlightKey(flight);

            expect(result).toEqual('2019-03-17JFKEI0034');
        });
    });

    describe('serializeFlight', () => {
        it('should return a byte buffer when valid flight supplied', () => {
            const flight = <ACRISFlight> {
                operatingAirline: {iataCode: 'EI'},
                departureAirport: 'JFK',
                arrivalAirport: "DUB",
                originDate: '2019-03-17',
                flightNumber: {trackNumber: '1234'}
            };

            const result = flightDataHelper.FlightDataHelper.serializeFlight(flight);

            expect(result).toEqual(jasmine.any(Buffer));
        });
    });

    describe('serializePaxCount', () => {
        it('should return a byte buffer when valid paxCount supplied', () => {
            const paxCount = <PaxCount> {
                category: 'total',
                elementType: 'pax',
                count: 23
            };

            const result = flightDataHelper.FlightDataHelper.serializeCount(paxCount);

            expect(result).toEqual(jasmine.any(Buffer));
        });
    });

    describe('bufferToObject', () => {
        it('should return a flight object when serialized bytes suplied', () => {
            const flight = <ACRISFlight> {
                operatingAirline: {iataCode: 'EI'},
                departureAirport: 'JFK',
                arrivalAirport: "DUB",
                originDate: '2019-03-17',
                flightNumber: {trackNumber: '1234'}
            };

            const serializedBytes = flightDataHelper.FlightDataHelper.serializeFlight(flight);
            const result = flightDataHelper.FlightDataHelper.bufferToObject(serializedBytes);

            expect(result).toEqual(flight);
        });

        it('should return null when invalid bytes suplied', () => {
            const result = flightDataHelper.FlightDataHelper.bufferToObject([17, 12, 15, 22, 32, 44]);

            expect(result).toEqual(null);
        });
    });

    describe('getPublicFlightData', () => {
        it ('should return core flight data', () => {
            const flight = <ACRISFlight> {
                operatingAirline: {iataCode: 'EI'},
                departureAirport: 'JFK',
                arrivalAirport: "DUB",
                originDate: '2019-03-17',
                flightNumber: {trackNumber: '1234',},
                extensions: { counts: { paxCounts: [ { elementType: 'pax', count: 12}, {elementType: 'bag', count: '11' } ]}}
            };
            const result = flightDataHelper.FlightDataHelper.getPublicFlightData(flight);

            expect(result.operatingAirline.iataCode).toEqual('EI');
        });

        it ('should remove any extension data', () => {
            const flight = <ACRISFlight> {
                operatingAirline: {iataCode: 'EI'},
                departureAirport: 'JFK',
                arrivalAirport: "DUB",
                originDate: '2019-03-17',
                flightNumber: {trackNumber: '1234',},
                extensions: { counts: { paxCounts: [ { elementType: 'pax', count: 12}, {elementType: 'bag', count: '11' } ]}}
            };

            const result = flightDataHelper.FlightDataHelper.getPublicFlightData(flight);

            expect(result.extensions).toBeNull();
        });
    });

    describe('getPaxCounts', () => {
       it ('should return empty array when no extensions data in flight', () => {
           const transientMap = new Map();

           const result = flightDataHelper.FlightDataHelper.getPaxCounts(transientMap);
           expect(result.length).toEqual(0);
       });

        it ('should return empty array when no count data in flight', () => {
            const transientMap = new Map();
            transientMap.set('counts', Buffer.from(JSON.stringify([])));

            const result = flightDataHelper.FlightDataHelper.getPaxCounts(transientMap);
            expect(result.length).toEqual(0);
        });

        it ('should return empty array when no pax count data in flight', () => {
            const transientMap = new Map();
            transientMap.set('counts', Buffer.from(JSON.stringify([ { elementType: 'bag', count: 12} ])));

            const result = flightDataHelper.FlightDataHelper.getPaxCounts(transientMap);
            expect(result.length).toEqual(0);
        });

        it ('should return array containing pax count when pax count data in flight', () => {
            const transientMap = new Map();
            transientMap.set('counts', Buffer.from(JSON.stringify([ { elementType: 'pax', count: 12} ])));

            const result = flightDataHelper.FlightDataHelper.getPaxCounts(transientMap);
            expect(result[0].count).toEqual(12);
        });

        it ('should should default category to "total" when pax count data in flight and no category specified', () => {
            const transientMap = new Map();
            transientMap.set('counts', Buffer.from(JSON.stringify([ { elementType: 'pax', count: 12} ])));

            const result = flightDataHelper.FlightDataHelper.getPaxCounts(transientMap);
            expect(result[0].category).toEqual('total');
        });
    });

    describe('getBagCounts', () => {
        it ('should return empty array when no extensions data in flight', () => {
            const transientMap = new Map();

            const result = flightDataHelper.FlightDataHelper.getBagCounts(transientMap);
            expect(result.length).toEqual(0);
        });

        it ('should return empty array when no count data in flight', () => {
            const transientMap = new Map();
            transientMap.set('counts', Buffer.from(JSON.stringify([])));

            const result = flightDataHelper.FlightDataHelper.getBagCounts(transientMap);
            expect(result.length).toEqual(0);
        });

        it ('should return empty array when no bag count data in flight', () => {
            const transientMap = new Map();
            transientMap.set('counts', Buffer.from(JSON.stringify([ { elementType: 'pax', count: 12} ])));

            const result = flightDataHelper.FlightDataHelper.getBagCounts(transientMap);
            expect(result.length).toEqual(0);
        });

        it ('should return array containing bag count when bag count data in flight', () => {
            const transientMap = new Map();
            transientMap.set('counts', Buffer.from(JSON.stringify([ { elementType: 'bag', count: 13} ])));

            const result = flightDataHelper.FlightDataHelper.getBagCounts(transientMap);
            expect(result[0].count).toEqual(13);
        });

        it ('should should default category to "total" when bag count data in flight and no category specified', () => {
            const transientMap = new Map();
            transientMap.set('counts', Buffer.from(JSON.stringify([ { elementType: 'bag', count: 13} ])));

            const result = flightDataHelper.FlightDataHelper.getBagCounts(transientMap);
            expect(result[0].category).toEqual('total');
        });
    });

    describe('getMergedPaxCounts', () => {
        it('should return empty when no pax counts in either supplied flights', () => {
            const existingPaxCounts = [  ];
            const transientMap = new Map();

            const result = flightDataHelper.FlightDataHelper.getMergedPaxCounts(existingPaxCounts, transientMap);
            expect(result.length).toEqual(0);
        });

        it('should return empty when no pax counts in updated flight', () => {
            const existingPaxCounts = [ { elementType: 'pax', count: 12} ];
            const transientMap = new Map();

            const result = flightDataHelper.FlightDataHelper.getMergedPaxCounts(existingPaxCounts, transientMap);
            expect(result.length).toEqual(0);
        });

        it('should return multiple counts when new pax count in updated flight', () => {
            const existingPaxCounts = [ { elementType: 'pax', count: 12} ];
            const transientMap = new Map();
            transientMap.set('counts', Buffer.from(JSON.stringify([ { elementType: 'pax', count: 12, assistanceQualifier: 'WCHR'} ])));

            const result = flightDataHelper.FlightDataHelper.getMergedPaxCounts(existingPaxCounts, transientMap);
            expect(result.length).toEqual(2);
        });

        it('should return overwritten counts when same pax count in updated flight', () => {
            const existingPaxCounts = [ { elementType: 'pax', category: 'total', count: 12} ];
            const transientMap = new Map();
            transientMap.set('counts', Buffer.from(JSON.stringify([ { elementType: 'pax', count: 13, category: 'total'} ])));

            const result = flightDataHelper.FlightDataHelper.getMergedPaxCounts(existingPaxCounts, transientMap);
            expect(result.length).toEqual(1);
            expect(result[0].count).toEqual(13);
        });

        it('should add count when new pax count in updated flight and none in existing', () => {
            const existingPaxCounts = null;
            const transientMap = new Map();
            transientMap.set('counts', Buffer.from(JSON.stringify([ { elementType: 'pax', count: 13, category: 'total'} ])));

            const result = flightDataHelper.FlightDataHelper.getMergedPaxCounts(existingPaxCounts, transientMap);
            expect(result.length).toEqual(1);
            expect(result[0].count).toEqual(13);
        });
    });

    describe('getMergedBagCounts', () => {
        it('should return empty when no bag counts in either supplied flights', () => {
            const existingPaxCounts = [  ];
            const transientMap = new Map();

            const result = flightDataHelper.FlightDataHelper.getMergedBagCounts(existingPaxCounts, transientMap);
            expect(result.length).toEqual(0);
        });

        it('should return empty when no bag counts in updated flight', () => {
            const existingPaxCounts = [ { elementType: 'pax', count: 12} ];
            const transientMap = new Map();

            const result = flightDataHelper.FlightDataHelper.getMergedBagCounts(existingPaxCounts, transientMap);
            expect(result.length).toEqual(0);
        });

        it('should return multiple counts when new bag count in updated flight', () => {
            const existingPaxCounts = [ { elementType: 'bag', count: 11} ];
            const transientMap = new Map();
            transientMap.set('counts', Buffer.from(JSON.stringify([ { elementType: 'bag', count: 1, transferQualifier: { airlineCode: 'EI', trackNumber: '123'} } ])));

            const result = flightDataHelper.FlightDataHelper.getMergedBagCounts(existingPaxCounts, transientMap);
            expect(result.length).toEqual(2);
        });

        it('should return overwritten counts when same bag count in updated flight', () => {
            const existingPaxCounts = [ { elementType: 'bag', category: 'total', count: 11} ];
            const transientMap = new Map();
            transientMap.set('counts', Buffer.from(JSON.stringify([ { elementType: 'bag', category: 'total', count: 14} ])));

            const result = flightDataHelper.FlightDataHelper.getMergedBagCounts(existingPaxCounts, transientMap);
            expect(result.length).toEqual(1);
            expect(result[0].count).toEqual(14);
        });

        it('should add count when new bag count in updated flight and none in existing', () => {
            const existingPaxCounts = null;
            const transientMap = new Map();
            transientMap.set('counts', Buffer.from(JSON.stringify([ { elementType: 'bag', category: 'total', count: 13} ])));

            const result = flightDataHelper.FlightDataHelper.getMergedBagCounts(existingPaxCounts, transientMap);
            expect(result.length).toEqual(1);
            expect(result[0].count).toEqual(13);
        });
    });
});
